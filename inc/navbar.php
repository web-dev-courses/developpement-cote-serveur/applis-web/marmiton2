<nav id="nav">
    <ul class="links">
        <li <?php echo ($page === 'index' ? 'class="active"' : ''); ?>>
            <a href="index.php" title="Accueil">
                <span>Accueil</span>
            </a>
        </li>
        
        <li <?php echo ($page === 'recettes' ? 'class="active"' : ''); ?>>
            <a href="recettes.php" title="Recettes">
                <span>Recettes</span>
            </a>
        </li>

        <li <?php echo ($page === 'ingredients' ? 'class="active"' : ''); ?>>
            <a href="ingredients.php" title="Ingrédients">
                <span>Ingrédients</span>
            </a>
        </li>

        <li <?php echo ($page === 'new-recette' ? 'class="active"' : ''); ?>>
            <a href="new-recette.php" title="Proposer une recette de ouf">
                <span>Proposer une nouvelle recette</span>
            </a>
        </li>
    </ul>

    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
    </ul>
</nav>