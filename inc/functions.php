
<?php

// -------------------------------
// FONCTIONS UTILES
// -------------------------------

// Construction de l'affichage de la durée de préparation en heures:minutes
function getDuree($duree) {
    $result = $duree . ' min';

    if ($duree >= 60) {
        $heures = (int)$duree / 60;
        $minutes = $duree % 60;
        $duree = $heures . ' h ' . ($minutes > 0 ? $minutes . ' min' : '');
    }

    return $result;
}


/**
 * Protège les caractères spéciaux d'une chaîne pour l'utiliser dans une requête SQL
 * Voir : http://php.net/manual/fr/mysqli.real-escape-string.php
 */
function cleanInput($mysqli, $input) {
    return $mysqli->real_escape_string($input);
}

// -------------------------------
// REQUÊTES A LA BASE DE DONNEES
// -------------------------------

/**
 * Récupération d'une recette à partir de l'id donné en paramètre
 */
function getRecetteById($mysqli, $id) {

    // Si l'id donné en paramètre n'est pas une entier c'est qu'il n'est pas valide,
    // on arrête donc l'exécution de la fonction en retournant NULL
    if (!is_int($id)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT recette.id, recette.intitule, recette.description,
            recette.duree_preparation,
            recette.duree_cuisson,
            recette.photo,
            categorie_recette.intitule AS C_intitule
        FROM recette
        INNER JOIN categorie_recette
            ON recette.categorie = categorie_recette.id
        WHERE recette.id = ' . $id . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $result = $query->fetch_assoc();

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $result;
}

/**
 * Récupération des étapes de préparation d'une recette donnée en paramètre
 */
function getPreparationRecetteById($mysqli, $id) {
    
    // Si l'id donné en paramètre n'est pas une entier c'est qu'il n'est pas valide,
    // on arrête donc l'exécution de la fonction en retournant NULL
    if (!is_int($id)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT description
        FROM preparation
        WHERE preparation.id_recette = ' . $id . '
        ORDER BY poids
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $result = $query->fetch_all(MYSQLI_ASSOC);
    
    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $result;
}
    
/**
 * Récupération des recettes
 */
function getRecettes($mysqli, $limit = null, $search = null) {
    // Construction de la requête SQL
    $sql = '
        SELECT recette.id, recette.intitule as R_intitule, recette.description,
            recette.duree_preparation + recette.duree_cuisson AS R_duree,
            recette.photo,
            categorie_recette.intitule AS C_intitule
        FROM recette
        INNER JOIN categorie_recette
            ON recette.categorie = categorie_recette.id
        ' . ($search !== null ? ' WHERE recette.intitule LIKE "%' . $search . '%" ' : '') . '
        ORDER BY date_ajout DESC
        ' . ($limit !== null ? ' LIMIT ' . $limit : '') . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }
    
    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}

/**
 * Récupération d'une ingredient à partir de l'id donné en paramètre
 */
function getCategoriesRecette($mysqli) {
    // Construction de la requête SQL
    $sql = '
        SELECT *
        FROM categorie_recette;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}

/**
 * Récupération d'une ingredient à partir de l'id donné en paramètre
 */
function getIngredientById($mysqli, $id) {
    
    if (!is_int($id)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT ingredient.id, ingredient.intitule, ingredient.description, ingredient.photo
        FROM ingredient
        WHERE ingredient.id = ' . $id . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $result = $query->fetch_assoc();

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $result;
}
    
/**
 * Récupération des ingrédients
 */
function getIngredients($mysqli, $limit = null) {
    // Construction de la requête SQL
    $sql = '
        SELECT ingredient.id, ingredient.intitule, ingredient.description,
               ingredient.photo, type_ingredient.intitule AS type
        FROM ingredient
        INNER JOIN type_ingredient
            ON ingredient.type = type_ingredient.id
        ORDER BY intitule ASC
        ' . ($limit !== null ? ' LIMIT ' . $limit : '') . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }
    
    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}

/**
 * Récupération des ingrédients d'une recette entrée en paramètre
 */
function getIngredientsByRecetteId($mysqli, $idRecette) {
    if (!is_int($idRecette)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT ingredient.id, ingredient.intitule, ingredient.photo
        FROM ingredient
        INNER JOIN r_recette_ingredient AS rri
            ON ingredient.id = rri.id_ingredient
        INNER JOIN recette
            ON recette.id = rri.id_recette
        WHERE recette.id = ' . $idRecette . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }
    
    // Récupération des résultats dans un tableau associatif
    $result = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $result;
}

/**
 * Traitement des données envoyées par le formulaire d'ajout d'une recette (contents/content-new-recette.php)
 */
function envoyerRecette($mysqli, $data) {
    $envoye = false;
    $base_photo = 'uploads/images/recettes/';

    // INFORMATIONS
    $intitule           = cleanInput($mysqli, $_POST['intitule']);
    $description        = cleanInput($mysqli, $_POST['description']);
    $nb_parts           = cleanInput($mysqli, $_POST['nb_parts']);
    $date_ajout         = new DateTime();
    $duree_preparation  = cleanInput($mysqli, $_POST['duree_preparation']);
    $duree_cuisson      = cleanInput($mysqli, $_POST['duree_cuisson']);
    $categorie          = cleanInput($mysqli, $_POST['categorie']);
    $photo              = $base_photo . cleanInput($mysqli, $_FILES['photo']['name']);
    
    $sql = '
        INSERT INTO recette 
            (intitule, description, nb_parts, date_ajout, duree_preparation, duree_cuisson, categorie, photo)
        VALUES (
            "' . $intitule . '",
            "' . $description . '",
            "' . $nb_parts . '",
            "' . $date_ajout->format('Y/m/d H:i:s') . '",
            "' . $duree_preparation . '",
            "' . $duree_cuisson . '",
            "' . $categorie . '",
            "' . $photo . '"
        );
    ';
    
    $envoye = $mysqli->query($sql);
    $idRecette = $mysqli->insert_id;

    // INGREDIENTS
    foreach ($_POST['ingredients'] as $idIngredient) {
        $sql = '
            INSERT INTO r_recette_ingredient VALUES (
                "' . $idRecette . '",
                "' . cleanInput($mysqli, $idIngredient) . '"           
            );
        ';

        $envoye = $mysqli->query($sql);        
    }

    // PREPARATION
    $poids = 1;
    foreach ($_POST['preparation'] as $etape) {
        $sql = '
            INSERT INTO preparation (id_recette, poids, description)
            VALUES (
                "' . $idRecette . '",
                "' . $poids . '",
                "' . cleanInput($mysqli, $etape) . '"           
            );
        ';

        $envoye = $mysqli->query($sql);

        $poids++;
    }

    // Si toutes les requêtes se sont bien exécutées, on récupère la photo de la recette
    if ($envoye) {
        move_uploaded_file($_FILES['photo']['tmp_name'], $base_photo . $_FILES['photo']['name']);        
    }

    return $envoye;
}