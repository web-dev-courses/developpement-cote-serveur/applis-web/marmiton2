<?php

$idRecette = (int)$_GET['id'];
$recette = getRecetteById($mysqli, $idRecette);

if ($recette === null) {
    echo "Aucune recette n'a été trouvée, veuillez revenir à la liste des recettes.";
} else {
?>

    <article class="recette image fit">
        <header>
            <h1><?php echo $recette['intitule']; ?></h1>
            <img src="<?php echo $recette['photo']; ?>" alt="" />
        </header>

        <div class="content">
            <div class="description">
                <?php echo $recette['description']; ?>
            </div>

            <div class="duree">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <?php echo getDuree($recette['duree_preparation'] + $recette['duree_cuisson']); ?>
            </div>
    
            <div class="ingredients">
                <h3>Ingrédients</h3>
                <ul>
                    <?php
                        $ingredients = getIngredientsByRecetteId($mysqli, $idRecette);

                        foreach ($ingredients as $ingredient) {
                    ?>

                        <li>
                            <a href="ingredient.php?id=<?php echo $ingredient['id']; ?>"
                               title="Accéder à la fiche de l'ingrédient &quot;<?php echo $ingredient['intitule'] ?>&quot;">
                                <img src="<?php echo $ingredient['photo']; ?>" alt="">
                                <span><?php echo $ingredient['intitule']; ?></span>
                            </a>
                        </li>
                        
                    <?php
                        }
                    ?>
                </ul>
            </div>

            <div class="preparation">
                <h3>Préparation</h3>
                <ul class="etapes">
                <?php
                    $etapes = getPreparationRecetteById($mysqli, $idRecette);
                    $i = 1;

                    if (!is_null($etapes)) {
                        foreach ($etapes as $etape) {
                        ?>

                            <li class="etape">
                                <h4>Etape <?php echo $i; ?></h4>
                                <?php echo $etape['description']; ?>
                            </li>

                        <?php
                            $i++;
                        }
                    }
                ?>
                </ul>
            </div>
        </div>

        <footer>
        </footer>
    </article>

<?php
}
?>
