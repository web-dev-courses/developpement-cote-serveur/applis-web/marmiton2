<?php

$idIngredient = (int)$_GET['id'];
$ingredient = getIngredientById($mysqli, $idIngredient);

if ($ingredient === null) {
    echo "Aucune ingredient n'a été trouvée, veuillez revenir à la liste des ingredients.";
} else {
?>

    <article class="ingredient image fit">
        <header>
            <h1><?php echo $ingredient['intitule']; ?></h1>
            <img src="<?php echo $ingredient['photo']; ?>" alt="" />
        </header>

        <div class="content">
            <?php echo $ingredient['description']; ?>
        </div>

        <footer>
        </footer>
    </article>

<?php
}
?>
