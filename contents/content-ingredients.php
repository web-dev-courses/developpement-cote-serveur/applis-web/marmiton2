<section id="ingredients" class="container">
<?php

$ingredients = getIngredients($mysqli);

if (count($ingredients) > 0) {
?>
    <div class="box alt">
        <div class="teasers-wrapper row 50% uniform">
            <?php
            // Itération sur les 3 autres ingredients récupérées
            foreach ($ingredients as $ingredient) {
            ?>
                <article class="teaser 4u">
                    <a href="ingredient.php?id=<?php echo $ingredient['id']; ?>"
                       title="Accéder à la fiche de l'ingredient &quote;<?php echo $ingredient['intitule'] ?>&quote;">
                        <header>
                            <h2><?php echo $ingredient['intitule']; ?></h2>
                            <span class="image fit">
                                <img src="<?php echo $ingredient['photo']; ?>" alt="" />
                            </span>
                        </header>

                        <div class="content">
                            <?php echo $ingredient['description']; ?>
                        </div>

                        <footer>
                            <span class="tag"><?php echo $ingredient['type']; ?></span>
                        </footer>
                    </a>
                </article>
            <?php
            }
            ?>

        </div>
    </div>

    <?php
    } else {
        echo 'Aucun ingrédient trouvé :(';
    }
    ?>

</section>
