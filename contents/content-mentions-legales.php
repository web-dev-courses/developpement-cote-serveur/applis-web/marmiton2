<h1>Mentions Légales</h1>

<h2>Confidentialité</h2>
<p>
    <span style="font-weight: bold;">SARL ADEM</span> n'enregistre pas d'informations personnelles permettant l'identification, à l'exception des formulaires
    que l'utilisateur est libre de remplir. Ces informations ne seront pas utilisées sans votre accord, nous les utiliserons
    seulement pour vous adresser des courriers, des brochures, des devis ou vous contacter.
    <br>
    <br>Les informations recueillies sur les sites bénéficient de la protection de la loi "Informatique et Libertés" n° 78-17
    du 06 janvier 1978. Elles bénéficient d'un droit d'accès, de rectification, d'opposition à communication et de suppression
    sur simple demande à
    <span style="font-weight: bold;">SARL ADEM</span>, 13 Bis Grande Route, 33430 Bernos-Beaulac.
    <br>
    <br>
    <span style="font-weight: bold;">SARL ADEM</span> pourra procéder à des analyses statistiques sans que celles-ci soient nominatives et pourra en informer
    des tiers (organismes d'évaluation de fréquentation) sous une forme résumée et non nominative.
    <br>
</p>
<h2>Utilisation de cookies</h2>
<p>
    <span style="font-weight: bold;"></span>La gestion des commandes nécessite l'utilisation de cookies. Des informations non personnelles sont enregistrées
    par ce système de cookies (fichiers texte utilisés pour reconnaître un utilisateur et ainsi faciliter son utilisation
    du site). Ceux-ci n'ont aucune signification en dehors de leur utilisation sur le site
    <a href="index.html">adem-demenagement.com</a>
</p>
<h2> Liens hypertexte</h2>
<p>
    <span style="font-weight: bold;">SARL ADEM</span> ne contrôle pas les sites en connexion avec le sien, et ne saurait donc être responsable de leur contenu.
    Les risques liés à l'utilisation de ces sites incombent pleinement à l'utilisateur. Il se conformera à leurs conditions
    d'utilisation.</p>
<h2>Editeur</h2>
<p>
    <span style="font-weight: bold;">SARL ADEM</span>
    <br>RCS Ville tribunal : 50210735200010
    <br>Siège social : 13 Bis Grande Route, 33430 Bernos-Beaulac.
    <br>N° de téléphone : 0556250144
    <br>N° de Fax : 0556250337</p>
<h2>Directeur de la publication</h2>
<p>
    <span style="font-weight: bold;">SARL ADEM</span>
</p>
<h2>Hébergement</h2>
<p>Linkeo.com
    <br>SA au capital de 700 000 euros
    <br>RCS Paris B 430 106 278
    <br>Siège social&nbsp;: 29 Rue du Colisee - 75008 Paris
    <br>N° de téléphone : 0826 46 20 20</p>
