<section id="recettes">
    <h1>Les dernières recettes</h1>

    <?php

    $recettes = getRecettes($mysqli, 4);

    if (count($recettes) > 0) {
        // Récupération de la première recette récupérée
        $recetteFirst = array_shift($recettes);

        // Récupération de la durée en heures:minutes
        $duree = getDuree($recetteFirst['R_duree']);
    ?>
        <article class="teaser first image fit">
            <a href="recette.php?id=<?php echo $recetteFirst['id']; ?>"
               title="Accéder à la recette &quot;<?php echo $recetteFirst['R_intitule']; ?>&quot;">
                <header>
                    <h2><?php echo $recetteFirst['R_intitule']; ?></h2>
                    <img src="<?php echo $recetteFirst['photo']; ?>" alt="" />
                </header>

                <div class="content">
                    <?php echo $recetteFirst['description']; ?>
                </div>

                <footer>
                    <span class="duree">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <?php echo $duree; ?>
                    </span>
                    <span class="tag">
                        <?php echo $recetteFirst['C_intitule']; ?>
                    </span>
                </footer>
            </a>
        </article>

        <div class="alt">
            <div class="teasers-wrapper row 50% uniform">
                <?php
                // Itération sur les 3 autres recettes récupérées
                foreach ($recettes as $recette) {
                    
                    // Récupération de la durée en heures:minutes
                    $duree = getDuree($recette['R_duree']);
                ?>
                    <article class="teaser 4u">
                        <a href="recette.php?id=<?php echo $recette['id']; ?>"
                           title="Accéder à la recette &quot;<?php echo $recette['R_intitule']; ?>&quot;">
                            <header>
                                <h2><?php echo $recette['R_intitule']; ?></h2>
                                <span class="image fit">
                                    <img src="<?php echo $recette['photo']; ?>" alt="" />
                                </span>
                            </header>

                            <div class="content">
                                <?php echo $recette['description']; ?>
                            </div>

                            <footer>
                                <span class="duree">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <?php echo $duree; ?>
                                </span>
                                <span class="tag">
                                    <?php echo $recette['C_intitule']; ?>
                                </span>
                            </footer>
                        </a>
                    </article>
                <?php
                }
                ?>

            </div>
        </div>

    <?php
    } else {
        echo 'Aucune recette trouvée :(';
    }
    ?>
</section>
