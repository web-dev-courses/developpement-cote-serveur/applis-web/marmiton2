<div class="row">
    <div class="col-sm-6">
        <img src="../img/404-bacon.png" alt="">
    </div>
    <div class="col-sm-6">
        <h2>Oups !</h2>
        <p>
            Il semblerait que cette page n'existe pas, je vous conseille de revenir sur la page des recettes pour trouver celle qui vous fera envie ! ;)
        </p>
    </div>
</div>
