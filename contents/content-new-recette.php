<?php
    // Variable indiquant si on doit récupérer les informations renseingées par l'utilisateur lors de son
    // dernier clic sur le bouton "Envoyer ma recette"
    $recup_infos = false;

    if (isset($_POST) && !empty($_POST)) {
        $envoye = envoyerRecette($mysqli, $_POST);

        if ($envoye) {
        ?>
            <div class="message success">
                Votre proposition de recette a bien été envoyée ! Notre communauté vous en sera éternellement reconnaissante.
            </div>            
        <?php
        } else {
            $recup_infos = true;
        ?>
            <div class="message error">
                Une erreur est survenue lors de l'envoi de votre proposition de recette, veuillez vérifier tous les champs
                puis réessayez. Merci.
            </div>
        <?php
        }
    }
?>

<h2 class="title">Proposer une nouvelle recette à notre large communauté de daleux !</h2>

<div class="f_new-recette">
    <form class="alt" action="new-recette.php" method="post" enctype="multipart/form-data">
        <h2>Informations</h2>

        <div class="row uniform">

            <div class="12u$">
                <label for="intitule">Intitulé de la recette</label>
                <input type="text" name="intitule" id="intitule"
                       value="<?php echo ($recup_infos ? $_POST['intitule'] : ''); ?>"
                       placeholder="Ex : Brandade de morue" required>
            </div>

            <div class="12u$">
                <label for="categorie">Vous proposez quoi comme recette ?</label>
                <select name="categorie" id="categorie" required>
                    <?php
                        $categories = getCategoriesRecette($mysqli);

                        foreach ($categories as $categorie) {
                    ?>
                            <option value="<?php echo $categorie['id'] ?>"
                                    title="<?php echo $categorie['description'] ?>"
                                    <?php echo ($recup_infos && $_POST['categorie'] == $categorie['id'] ? 'selected' : ''); ?>
                            >
                                <?php echo $categorie['intitule']; ?>
                            </option>
                    <?php
                        }
                    ?>
                </select>
            </div>

            <div class="12u$">
                <label for="description">Description</label>
                <textarea name="description" id="description" cols="30" rows="10"
                          placeholder="Description courte pour nous donner envie !"
                          required
                ><?php echo ($recup_infos ? $_POST['description'] : ''); ?></textarea>
            </div>
            
            <div class="4u 12u$(small)">
                <label for="duree_preparation">Durée pour la préparation (minutes)</label>
                <input type="text" name="duree_preparation" id="duree_preparation"
                       placeholder="Ex : 70 pour 1h et 10min" required
                       value="<?php echo ($recup_infos ? $_POST['duree_preparation'] : ''); ?>"
                >
            </div>

            <div class="4u 12u$(small)">
                <label for="duree_cuisson">Durée pour la cuisson (minutes)</label>
                <input type="text" name="duree_cuisson" id="duree_cuisson"
                       placeholder="Ex : 70 pour 1h et 10min" required
                       value="<?php echo ($recup_infos ? $_POST['duree_cuisson'] : ''); ?>"
                >
            </div>

            <div class="4u$ 12u$(small)">
                <label for="nb_parts">Nombre de parts</label>
                <input type="text" name="nb_parts" id="nb_parts"
                    placeholder="Ex : 4 pour 4 personnes" required
                    value="<?php echo ($recup_infos ? $_POST['nb_parts'] : ''); ?>"
                >
            </div>

            <div class="12u$">
                <label for="photo">Une magnifique photo pour nous faire baver</label>
                <input type="file" name="photo" id="photo" required>
            </div>
        
        </div>

        <h2>Ingrédients</h2>
        
        <div class="ingredients row uniform">
            <div class="12u$">
                <label for="ingredients">Selectionnez les ingrédients dans la liste ci-dessous</label>
                <select class="ingredients select2" name="ingredients[]" id="ingredients" multiple="multiple" required>
                    <?php
                        $ingredients = getIngredients($mysqli);

                        foreach ($ingredients as $ingredient) {
                    ?>
                            <option value="<?php echo $ingredient['id'] ?>"
                                    title="<?php echo $ingredient['description'] ?>"
                                    data-image="<?php echo $ingredient['photo']; ?>"
                                    <?php echo ($recup_infos && in_array($ingredient['id'], $_POST['ingredients']) ? 'selected' : ''); ?>
                            >
                                <?php echo $ingredient['intitule']; ?>
                            </option>
                    <?php
                        }
                    ?>
                </select>
            </div>
        </div>

        <h2>Préparation</h2>
        <div class="preparation row uniform">
            <div class="12u$">
                <ul id="etapes" class="sortable">
                    <li class="etape">
                        <div class="header">
                            <i class="handle fa fa-arrows"></i>
                            Etape <span class="weight">1</span>
                            <button title="Supprimer cette étape"
                                    class="remove-item icon fa fa-trash"
                                    data-parent="etapes"></button>
                        </div>
                        <textarea name="preparation[]"
                                  required
                        ><?php echo ($recup_infos && is_array($_POST['preparation']) ? $_POST['preparation'][0] : ''); ?></textarea>
                    </li>

                    <?php
                        // Si l'envoi de la recette n'a pas fonctionné on reconstruit les étapes de préparation qui avaient été
                        // renseignées par l'utilisateur afin de ne pas lui imposer de tout refaire (ergonomie ++ !)
                        if ($recup_infos && is_array($_POST['preparation']) && count($_POST['preparation']) > 1) {
                            $etapes = $_POST['preparation'];

                            for ($i = 1; $i < count($etapes); $i++) {
                                $etape = $etapes[$i];
                    ?>
                                <li class="etape">
                                    <div class="header">
                                        <i class="handle fa fa-arrows"></i>
                                        Etape <span class="weight"><?php echo $i + 1; ?></span>
                                        <button title="Supprimer cette étape"
                                                class="remove-item icon fa fa-trash"
                                                data-parent="etapes"></button>
                                    </div>
                                    <textarea name="preparation[]" required><?php echo $etape; ?></textarea>
                                </li>
                    <?php
                            }
                        }
                    ?>
                </ul>
                <button class="add-item button icon fa fa-plus" data-target="etapes" data-model="modele-etape">Ajouter une étape</button>
            </div>
        </div>
            
        <div class="submit row uniform">
            <div class="12u$">
                <input type="submit" value="Envoyer ma recette !">
            </div>
        </div>
    </form>
</div>
