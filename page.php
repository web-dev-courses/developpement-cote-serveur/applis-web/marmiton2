<?php
    include 'inc/head.php';
?>

<body id="<?php echo ($page ? $page : 'page404'); ?>" class="is-loading">
    <div id="wrapper" class="fade-in">

        <?php
            if ($page === 'index') {
        ?>

            <div id="intro">
                <h1>Prêt pour<br />La Graille !??</h1>
                <p>Fini le ventre qui rumine, ça va grailler dur !</p>
                <ul class="actions">
                    <li><a href="#header" class="button icon solo fa-arrow-down scrolly">Miam !</a></li>
                </ul>
            </div>

        <?php
            }
        ?>
        <?php include 'inc/header.php'; ?>

        <div id="main">
            <div id="content">
                <?php include 'contents/content-' . ($page ? $page : '404') . '.php'; ?>
            </div>
        </div>

        <?php include 'inc/footer.php'; ?>

    </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.scrollex.min.js"></script>
    <script src="assets/js/jquery.scrolly.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/lib/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/lib/select2/select2.full.min.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>
</html>
